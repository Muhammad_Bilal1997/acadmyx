package com.example.acadmyx;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.os.ConfigurationCompat;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.acadmyx.HelperClass.CountryData;

import java.sql.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Locale;

import static com.example.acadmyx.HelperClass.CountryData.countryNames;

public class PhoneNumberAuthenticationScreen extends AppCompatActivity {

    private EditText phoneNumber;
    private String[] countriesNames;
    int countryNamePosition;
    Locale locale;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_number_authentication_screen);

        init();

        //Now load all country code

        TelephonyManager tm = (TelephonyManager)this.getSystemService(Context.TELEPHONY_SERVICE);
        String countryCodeValue = tm.getNetworkCountryIso();

        Locale loc = new Locale("", countryCodeValue);
        String countryName=loc.getDisplayCountry();

        CountryData countryData = new CountryData();
        countriesNames=countryData.CountriesNames();

        countryNamePosition = Arrays.asList(countriesNames).indexOf(countryName);



        findViewById(R.id.buttonContinue).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String countryCode = CountryData.countryAreaCodes[countryNamePosition];
                String number = phoneNumber.getText().toString().trim();
                if(number.isEmpty() || number.length() < 10){
                    phoneNumber.setError("Number is Required!");
                    phoneNumber.requestFocus();
                    return;
                }
                String phoneNumber = "+" + countryCode + number;

                Intent intent = new Intent(getApplicationContext(), VerifyPhone.class);
                intent.putExtra("phonenumber", phoneNumber);
                startActivity(intent);
            }
        });


    }


    private void init() {
        // Initialize above fields and connection between xml and java
        phoneNumber = findViewById(R.id.editTextPhone);
    }

}
