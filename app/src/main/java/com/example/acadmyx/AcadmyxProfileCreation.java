package com.example.acadmyx;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.example.acadmyx.HelperClass.CountryData;
import com.example.acadmyx.HelperClass.LoginRole;
import com.example.acadmyx.HelperClass.UserInfo;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class AcadmyxProfileCreation extends AppCompatActivity {

    private Button createProfile;
    private Spinner professionSpinner;
    private EditText userName, userAddress;
    private String professionAs;
    private DatabaseReference acadmyxUserProfile;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acadmyx_profile_creation);

        //initialization func
        init();

        //Now load professions into spinner
        professionSpinner.setAdapter(new ArrayAdapter<String>
                (getApplicationContext(),R.layout.support_simple_spinner_dropdown_item, LoginRole.LoginAs));
        createProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                buttonCallsFunc();
            }
        });

    }

    private void buttonCallsFunc() {
        
        String Name = userName.getText().toString();
        String Address = userAddress.getText().toString();
        professionAs = LoginRole.LoginAs[professionSpinner.getSelectedItemPosition()];
        if(Name.isEmpty() || Address.isEmpty() || professionAs.isEmpty()){
            Toast.makeText(AcadmyxProfileCreation.this, "Some Fields are Empty!", Toast.LENGTH_SHORT).show();
        }else{
            createUserProfile();
        }
           
    }

    private void createUserProfile() {
        String Name = userName.getText().toString();
        String Address = userAddress.getText().toString();
        professionAs = LoginRole.LoginAs[professionSpinner.getSelectedItemPosition()];
        UserInfo userInfo = new UserInfo(Name,Address,professionAs);
        acadmyxUserProfile = FirebaseDatabase.getInstance().getReference().child(professionAs).child(CountryData.USER_ID).child("profile");
        acadmyxUserProfile.setValue(userInfo).addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    startActivity(new Intent(getApplicationContext(),MainActivity.class));
                    finish();
                }else{
                    Toast.makeText(AcadmyxProfileCreation.this, "Something went wrong!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void init() {
        createProfile = findViewById(R.id.createprofile);
        professionSpinner = findViewById(R.id.professionspinner);
        userName = findViewById(R.id.email);
        userAddress = findViewById(R.id.address);
        CountryData.USER_ID = FirebaseAuth.getInstance().getUid();
    }
}
