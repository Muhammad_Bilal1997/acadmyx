package com.example.acadmyx;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.acadmyx.HelperClass.CountryData;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.SignInMethodQueryResult;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class AcadmyxStudenyLogin extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {

    SignInButton signInButton;
    Button phoneAuth,phoneScreenBtn,studentBtn,teacherBtn;
    private GoogleApiClient googleApiClient;
    private static final int SIGN_IN = 1;
    private FirebaseAuth auth;
    private Button signIn;
    private EditText email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_acadmyx_login);

        init();


        //Google Sign in Button
        signInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = Auth.GoogleSignInApi.getSignInIntent(googleApiClient);
                startActivityForResult(intent,SIGN_IN);
            }
        });

        //Go to phone authentication Screen
        phoneAuth.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(AcadmyxStudenyLogin.this, "Phone Verification Screen", Toast.LENGTH_SHORT).show();
                startActivity(new Intent(getApplicationContext(),PhoneNumberAuthenticationScreen.class));
                finish();
            }
        });

        //student Button Action
        studentBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CountryData.Profession = studentBtn.getText().toString();
            }
        });

        //Teacher Button Action
        teacherBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CountryData.Profession = teacherBtn.getText().toString();
            }
        });
        
        //Manually Sign In
        signIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                CountryData.USER_EMAIL = email.getText().toString().trim();
                isEmailExist();
            }
        });

    }

    private void init() {
        //Google Sign in Options

        GoogleSignInOptions googleSignInOptions =  new
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN).requestEmail().build();

        //Initailize Google Api client

        googleApiClient = new GoogleApiClient.Builder(this).enableAutoManage(this,this)
                .addApi(Auth.GOOGLE_SIGN_IN_API , googleSignInOptions).build();

        //Link buttons to XML file

        signInButton = findViewById(R.id.signInButton);
        phoneAuth = findViewById(R.id.phoneAuthbtn);
        studentBtn = findViewById(R.id.student);
        teacherBtn = findViewById(R.id.teacher);
        signIn = findViewById(R.id.signin);
        email = findViewById(R.id.email);

        //Firebase Auth

        auth = FirebaseAuth.getInstance();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Toast.makeText(this, "Check1", Toast.LENGTH_SHORT).show();
        if(requestCode ==  SIGN_IN){
            GoogleSignInResult googleResult = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            GoogleSignInResult result =
                    Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            GoogleSignInAccount acct = result.getSignInAccount();
            if(googleResult.isSuccess()){
                CountryData.USER_ID = acct.getId();
                CountryData.USER_EMAIL = acct.getEmail();
                Toast.makeText(this, ""+CountryData.Profession, Toast.LENGTH_SHORT).show();
                if(CountryData.Profession.isEmpty()) {
                    Toast.makeText(this, "Kindly Select Profession", Toast.LENGTH_SHORT).show();
                }else{
                    isProfileExist();
                }


            }else {
                Toast.makeText(this, "Login has Failed !", Toast.LENGTH_SHORT).show();
            }
        }
    }

    private boolean isEmailExist() {
        FirebaseAuth.getInstance().fetchSignInMethodsForEmail(CountryData.USER_EMAIL).addOnCompleteListener(new OnCompleteListener<SignInMethodQueryResult>() {
            @Override
            public void onComplete(@NonNull Task<SignInMethodQueryResult> task) {
                if(task.getResult().getSignInMethods().isEmpty()){
                    startActivity(new Intent(getApplicationContext(), AcadmyxProfileCreation.class));
                    Toast.makeText(AcadmyxStudenyLogin.this, "Not Found", Toast.LENGTH_SHORT).show();
                }else{
                    //startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    Toast.makeText(getApplicationContext(), "Email is already exist!", Toast.LENGTH_SHORT).show();
                }
            }
        });
        return true;
    }


    private void isProfileExist() {
        //Check user id is exist or not?
        Log.d("ABCD","check"+CountryData.USER_ID);

        DatabaseReference userRef = FirebaseDatabase.getInstance().getReference().child(CountryData.Profession).child(CountryData.USER_ID);
        userRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                if(dataSnapshot != null){
                    if(dataSnapshot.getValue() == null){
                        startActivity(new Intent(getApplicationContext(), AcadmyxProfileCreation.class));
                        finish();
                    }
                    else{
                        startActivity(new Intent(getApplicationContext(), MainActivity.class));
                        finish();
                    }
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {

            }
        });
    }


}
