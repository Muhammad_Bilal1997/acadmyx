package com.example.acadmyx.HelperClass;

public class UserInfo {
    String Name, Address, Profession;

    public UserInfo() {
    }

    public UserInfo(String name, String address, String profession) {
        Name = name;
        Address = address;
        Profession = profession;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getProfession() {
        return Profession;
    }

    public void setProfession(String profession) {
        Profession = profession;
    }
}
